# dvdcpy

dvdcpy is a librarification of [ogmrip](https://ogmrip.sourceforge.net/en/index.html)'s
dvdcpy command-line utility, which you can use to rip DVDs to disk, on
order to remove the copy protection, or store them as backups on a
network attached storage.

Testing
-------

1) Install and setup cdemu as per:
   https://copr.fedorainfracloud.org/coprs/rok/cdemu/

2) Reboot to have the kernel modules recreated

3) "Rip" a CSS protected DVD:
   dd if=/dev/sr1 of=dvd.iso status=progress

4) Emulate the DVD with cdemu:
   cdemu load 0 dvd.iso --dvd-report-css

5) Run your tests!
   ./dvdcpy /dev/sr0 -o orig-iso -m
   vs.
   ./dvdcpy /dev/sr0 -d -o lib-iso -m
