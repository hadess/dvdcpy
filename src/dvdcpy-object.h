/* dvdcpy - A DVD backup tool
 * Copyright (C) 2004-2009 Olivier Rolland <billl@users.sourceforge.net>
 * Copyright (C) 2010 Bastien Nocera <hadess@hadess.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __DVDCPY_OBJECT_H__
#define __DVDCPY_OBJECT_H__

#include <gio/gio.h>

G_BEGIN_DECLS

typedef enum
{
  DVD_DATA_TYPE_UNSET = 0,
  DVD_DATA_TYPE_IFO,
  DVD_DATA_TYPE_BUP,
  DVD_DATA_TYPE_VOB,
  DVD_DATA_TYPE_MENU
} DvdDataType;

#define DVDCPY_TYPE_OBJECT         (dvdcpy_object_get_type ())

G_DECLARE_FINAL_TYPE (DvdcpyObject, dvdcpy_object, DVDCPY, OBJECT, GObject)

typedef enum
{
  BAD_BLOCK_STRATEGY_ABORT,
  BAD_BLOCK_STRATEGY_BLANK,
  BAD_BLOCK_STRATEGY_SKIP
} BadBlockStrategy;

DvdcpyObject *   dvdcpy_object_new            (void);

void             dvdcpy_set_debug             (gboolean         debug);
gboolean         dvdcpy_object_open           (DvdcpyObject     *obj,
                                               GFile            *device,
                                               GList            *titles, //FIXME document
                                               GError           **error);
void             dvdcpy_object_close          (DvdcpyObject     *obj);

gboolean         dvdcpy_object_copy           (DvdcpyObject     *obj,
                                               GFile            *path,
                                               GError           **error);

void             dvdcpy_object_set_strategy   (DvdcpyObject     *obj,
                                               BadBlockStrategy strategy);
BadBlockStrategy dvdcpy_object_get_strategy   (DvdcpyObject     *obj);
void             dvdcpy_object_set_copy_menus (DvdcpyObject     *obj,
                                               gboolean         with_menus);

G_END_DECLS

#endif /* __DVDCPY_OBJECT_H__ */

