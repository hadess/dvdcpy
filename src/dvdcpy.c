/* dvdcpy - A DVD backup tool
 * Copyright (C) 2004-2010 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <getopt.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#include <sys/stat.h>

#include <dvdread/dvd_reader.h>
#include <dvdread/ifo_read.h>

#include "dvdcpy-object.h"

#define CLAMP(x, low, high)  (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))

static BadBlockStrategy strategy = BAD_BLOCK_STRATEGY_ABORT;

/*
 * Usage
 */

static void
usage (const char *name)
{
  fprintf (stdout, "Usage:\n");
  fprintf (stdout, "  %s [OPTION...] <DVD DEVICE>\n", name);
  fprintf (stdout, "\n");
  fprintf (stdout, "Help Options:\n");
  fprintf (stdout, "  -h, --help                Show help options\n");
  fprintf (stdout, "\n");
  fprintf (stdout, "Application Options:\n");
  fprintf (stdout, "  -o, --output output       Specify the output directory\n");
  fprintf (stdout, "  -t, --title title         Select the title to copy. This option may be\n");
  fprintf (stdout, "                            specified multiple times (default: all titles)\n");
  fprintf (stdout, "  -s, --strategy strategy   Select the strategy to use on error: skip, blank or\n");
  fprintf (stdout, "                            abort (default: abort)\n");
  fprintf (stdout, "  -m, --menu                Copy the DVD menu\n");
  fprintf (stdout, "  -d, --debug               Enable debug output\n");
  fprintf (stdout, "\n");
}

static void
skipped_cb (DvdcpyObject *obj, guint64 skip, guint64 offset, gpointer data)
{
  BadBlockStrategy strategy;

  strategy = dvdcpy_object_get_strategy (obj);
  fprintf (stderr, "Warning: %s %" G_GUINT64_FORMAT " blocks at offset %" G_GUINT64_FORMAT "\n",
      strategy == BAD_BLOCK_STRATEGY_SKIP ? "Skipped" : "Blanked", skip, offset);
}

static void
progress_cb (DvdcpyObject *obj, DvdDataType type, guint64 copied, guint64 total_size, gpointer data)
{
  const char *type_str;
  DvdDataType *last_type = data;

  switch (type)
  {
    case DVD_DATA_TYPE_IFO:
      type_str = "Metadata";
      break;
    case DVD_DATA_TYPE_BUP:
      type_str = "Backup metadata";
      break;
    case DVD_DATA_TYPE_VOB:
      type_str = "Video";
      break;
    case DVD_DATA_TYPE_MENU:
      type_str = "Menus";
      break;
    default:
      g_assert_not_reached ();
  }

  if (*last_type == type)
    fprintf (stdout, "\r");
  else
    fprintf (stdout, "\n");
  fprintf (stdout, "%" G_GUINT64_FORMAT "/%" G_GUINT64_FORMAT " blocks written (%" G_GUINT64_FORMAT "%%) (%s)",
      copied, total_size, copied * 100 / total_size, type_str);
  fflush (stdout);

  *last_type = type;
}

static const char *shortopts = "hmo:s:t:d";
static const struct option longopts[] =
{
  { "help",     no_argument,       NULL, 'h' },
  { "menu",     no_argument,       NULL, 'm' },
  { "output",   required_argument, NULL, 'o' },
  { "strategy", required_argument, NULL, 's' },
  { "title",    required_argument, NULL, 't' },
  { "debug",    no_argument,       NULL, 'd' },
  { NULL,       0,                 NULL,  0  }
};

int
main (int argc, char *argv[])
{
  unsigned int vmg, title;

  int c, optidx, ret;
  char *output;
  DvdcpyObject *obj;
  DvdDataType last_type = DVD_DATA_TYPE_UNSET;

  GFile *file = NULL;
  GList *titles = NULL;
  GError *error = NULL;

  if (argc < 2)
  {
    usage (argv[0]);
    return EXIT_FAILURE;
  }

  title = vmg = 0;
  output = "backup";
  ret = EXIT_FAILURE;

  while ((c = getopt_long (argc, argv, shortopts, longopts, &optidx)) != EOF)
  {
    switch (c)
    {
      case 'd':
        dvdcpy_set_debug (TRUE);
        break;
      case 'h':
        usage (argv[0]);
        return EXIT_SUCCESS;
        break;
      case 'm':
        vmg = 1;
        break;
      case 'o':
        output = optarg;
        break;
      case 's':
        if (strcmp (optarg, "blank") == 0)
          strategy = BAD_BLOCK_STRATEGY_BLANK;
        else if (strcmp (optarg, "skip") == 0)
          strategy = BAD_BLOCK_STRATEGY_SKIP;
        else if (strcmp (optarg, "abort") == 0)
          strategy = BAD_BLOCK_STRATEGY_ABORT;
        else
        {
          fprintf (stderr, "Error: unknown strategy: %s\n", optarg);
          return EXIT_FAILURE; 
        }
        break;
      case 't':
        title = atoi (optarg);
        if (title < 1 || title > 99)
        {
          fprintf (stderr, "Error: title must be greater than 0 and lesser than 100\n");
          return EXIT_FAILURE; 
        }
        titles = g_list_prepend (titles, GUINT_TO_POINTER (title));
        break;
      default:
        usage (argv[0]);
        return EXIT_FAILURE;
        break;
    }
  }

  if (optind != argc - 1)
  {
    usage (argv[0]);
    return EXIT_FAILURE;
  }

  obj = dvdcpy_object_new ();

  g_signal_connect (obj, "skipped",
      G_CALLBACK (skipped_cb), NULL);
  g_signal_connect (obj, "progress",
      G_CALLBACK (progress_cb), &last_type);

  file = g_file_new_for_path (argv[optind]);
  if (dvdcpy_object_open (obj, file, titles, &error) == FALSE)
  {
    fprintf (stderr, "Error: Cannot open DVD device\n" "%s", error->message);
    goto cleanup;
  }

  g_object_unref (file);

  /* Set whether we copy the menus */
  if (!titles)
    vmg = 1;
  dvdcpy_object_set_copy_menus (obj, vmg);

  dvdcpy_object_set_strategy (obj, strategy);

  file = g_file_new_for_commandline_arg (output);
  if (dvdcpy_object_copy (obj, file, &error) == FALSE)
  {
    fprintf (stderr, "Error: An error occured copying the DVD\n" "%s\n", error->message);
    goto cleanup;
  }

  fprintf (stdout, "Copy completed successfully\n");

  ret = EXIT_SUCCESS;

cleanup:

  if (obj)
    g_object_unref (obj);

  if (file)
    g_object_unref (file);

  if (error)
    g_error_free (error);

  return ret;
}

