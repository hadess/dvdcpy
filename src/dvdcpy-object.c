/* dvdcpy - A DVD backup tool
 * Copyright (C) 2004-2009 Olivier Rolland <billl@users.sourceforge.net>
 * Copyright (C) 2010, 2012 Bastien Nocera <hadess@hadess.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <gio/gio.h>
#include <glib/gi18n.h>


#include <dvdread/dvd_reader.h>
#include <dvdread/ifo_read.h>

#include "dvdcpy-object.h"

static gboolean debug_enabled = FALSE;

#define LOG(x) if (debug_enabled) x;

struct _DvdcpyObject
{
  /* Status */
  gboolean is_opened;
  goffset size;
  gboolean is_copying;
  guint64 copied;
  char *output;

  /* DVD stuff */
  dvd_reader_t *reader;
  ifo_handle_t *vmg_file;
  guint titles[100];
  guint vtss[100];

  /* Options */
  BadBlockStrategy strategy;
  gboolean with_menus;
};

struct _DvdcpyObjectClass
{
  GObjectClass parent_class;
};

enum
{
  SKIPPED_SIGNAL,
  PROGRESS_SIGNAL,
  LAST_SIGNAL
};

static guint signals [LAST_SIGNAL] = { 0, };

static void     dvdcpy_object_finalize   (GObject           *object);

G_DEFINE_FINAL_TYPE (DvdcpyObject, dvdcpy_object, G_TYPE_OBJECT)

static goffset
dvd_file_size (dvd_reader_t *reader, guint vts, dvd_read_domain_t domain)
{
  dvd_stat_t statbuf;

  if (DVDFileStat (reader, vts, domain, &statbuf) < 0)
  {
    LOG (fprintf (stderr, "Error: Cannot stat VOB for VTS %u\n", vts));
    return -1;
  }

  return statbuf.size / (uint32_t) DVD_VIDEO_LB_LEN;
}

static gboolean
get_space_left (GFile *file, guint64 *size)
{
  GFileInfo *info;
  GError *error = NULL;

  if (debug_enabled)
  {
    char *uri;
    uri = g_file_get_uri (file);
    fprintf (stdout, "Checking free space on %s\n", uri);
    g_free (uri);
  }

  info = g_file_query_filesystem_info (file, G_FILE_ATTRIBUTE_FILESYSTEM_FREE, NULL, &error);
  if (!info)
  {
    if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND))
    {
      GFile *parent;
      gboolean ret;

      LOG(fprintf (stderr, "Free space check failed, file doesn't exist\n"));

      g_error_free (error);

      parent = g_file_get_parent (file);
      if (!parent)
        return FALSE;

      ret = get_space_left (parent, size);
      g_object_unref (parent);

      if (!ret)
        return FALSE;
    }
    else
    {
      LOG (fprintf (stderr, "Failed to query filesystem: %s", error->message));
      g_error_free (error);
      return FALSE;
    }
  }
  else
  {
    *size = g_file_info_get_attribute_uint64 (info, G_FILE_ATTRIBUTE_FILESYSTEM_FREE);
    g_object_unref (info);
  }

  return TRUE;
}

static gboolean
make_directory (GFile *dir, GError **error)
{
  gchar *dirname;

  dirname = g_file_get_path (dir);
  if (g_mkdir_with_parents (dirname, 0755) < 0)
  {
    int err = errno;
    g_set_error (error, G_IO_ERROR, g_io_error_from_errno (err),
        _("Failed to create output directory"));
    g_free (dirname);
    return FALSE;
  }
  g_free (dirname);

  return TRUE;
}

static gssize
read_blocks (DvdcpyObject *obj, dvd_file_t *file, gsize offset, gsize size, guchar *buffer)
{
  gssize count;
  gsize skip;

  count = 0;

  for (skip = 0; skip < size; skip ++)
  {
    count = DVDReadBlocks (file, offset + skip, size - skip, buffer);
    if (count > 0)
      break;

    if (obj->strategy == BAD_BLOCK_STRATEGY_ABORT)
      return count;

    if (obj->strategy == BAD_BLOCK_STRATEGY_BLANK)
      memset (buffer + skip, 0, DVD_VIDEO_LB_LEN);
  }

  if (skip)
    g_signal_emit (obj, signals[SKIPPED_SIGNAL], 0, skip, offset);

  return count + skip;
}

static gssize
write_blocks (GOutputStream *stream, const void *buffer, gsize nblocks)
{
  gssize written, total_written;
  gsize nbytes;

  total_written = 0;
  nbytes = nblocks * DVD_VIDEO_LB_LEN;

  while (total_written != nbytes)
  {
    written = g_output_stream_write (stream, buffer + total_written, nbytes - total_written, NULL, NULL);
    if (written < 0)
      return -1;
    total_written += written;
  }

  return nblocks;
}

static ssize_t
dvd_copy_blocks (DvdcpyObject *obj, dvd_file_t *file, gsize offset, gsize size, GFileOutputStream *stream, DvdDataType type)
{
  guchar buffer[512 * DVD_VIDEO_LB_LEN];
  gssize count, len;
  guint start;

  for (count = 512, start = offset; offset < start + size; offset += count)
  {
    if (offset + count > start + size)
      count = start + size - offset;
    len = count;

    count = read_blocks (obj, file, offset, len, buffer);
    if (count < 0)
    {
      LOG(fprintf (stderr, "Error: Cannot read data\n"));
      return -1;
    }

    if (write_blocks (G_OUTPUT_STREAM (stream), buffer, count) < 0)
    {
      LOG(fprintf (stderr, "Error: Cannot write data: %s\n", strerror (errno)));
      return -2;
    }

    obj->copied += count;
    g_signal_emit (obj, signals[PROGRESS_SIGNAL], 0, type, obj->copied, obj->size);
  }

  g_signal_emit (obj, signals[PROGRESS_SIGNAL], 0, type, obj->copied, obj->size);

  return size;
}

/*
 * Get size functions
 */

static goffset
dvd_get_vts_size (dvd_reader_t *reader, guint vts)
{
  goffset size, fullsize;

  if ((size  = dvd_file_size (reader, vts, DVD_READ_INFO_FILE)) < 0)
    return -1;
  fullsize = size;

  if ((size = dvd_file_size (reader, vts, DVD_READ_INFO_BACKUP_FILE)) > 0)
    fullsize += size;

  if ((size = dvd_file_size (reader, vts, DVD_READ_MENU_VOBS)) > 0)
    fullsize += size;

  if (vts > 0)
  {
    if ((size = dvd_file_size (reader, vts, DVD_READ_TITLE_VOBS)) < 0)
      return -1;
    fullsize += size;
  }

  return fullsize;
}

static goffset
dvd_get_vmg_size (dvd_reader_t *reader)
{
  goffset size, fullsize;

  if ((size  = dvd_file_size (reader, 0, DVD_READ_INFO_FILE)) < 0)
    return -1;
  fullsize = size;

  if ((size = dvd_file_size (reader, 0, DVD_READ_INFO_BACKUP_FILE)) > 0)
    fullsize += size;

  if ((size = dvd_file_size (reader, 0, DVD_READ_MENU_VOBS)) > 0)
    fullsize += size;

  return fullsize;
}

static goffset
dvd_get_size (DvdcpyObject *obj)
{
  goffset fullsize, size;
  guint vts;

  if ((fullsize = dvd_get_vmg_size (obj->reader)) < 0)
    return -1;

  for (vts = 0; vts < obj->vmg_file->vmgi_mat->vmg_nr_of_title_sets; vts ++)
  {
    if (obj->vtss[vts])
    {
      if ((size = dvd_get_vts_size (obj->reader, vts + 1)) < 0)
        return -1;
      fullsize += size;
    }
  }

  return fullsize;
}

/*
 * Copy functions
 */

static gssize
dvd_copy_ifo (DvdcpyObject *obj, guint vts)
{
  GFile *gfile;
  GFileOutputStream *stream;

  gchar filename[FILENAME_MAX + 1];
  gssize size, copied;

  dvd_file_t *file;

  if ((size = dvd_file_size (obj->reader, vts, DVD_READ_INFO_FILE)) <= 0)
    return size;

  if (vts == 0)
    snprintf (filename, FILENAME_MAX, "%s/VIDEO_TS/VIDEO_TS.IFO", obj->output);
  else
    snprintf (filename, FILENAME_MAX, "%s/VIDEO_TS/VTS_%02u_0.IFO", obj->output, vts);

  gfile = g_file_new_for_path (filename);
  stream = g_file_replace (gfile, NULL, FALSE, 0, NULL, NULL);
  g_object_unref (gfile);

  if (!stream)
  {
    LOG(fprintf (stderr, "Error: Cannot open %s\n", filename));
    return -1;
  }

  file = DVDOpenFile (obj->reader, vts, DVD_READ_INFO_FILE);
  if (!file)
  {
    LOG(fprintf (stderr, "Error: Cannot open IFO for VTS %u\n", vts));
    g_object_unref (stream);
    return -1;
  }

  LOG(fprintf (stdout, "Copying IFO for VTS %u\n", vts));
  copied = dvd_copy_blocks (obj, file, 0, size, stream, DVD_DATA_TYPE_IFO);
  LOG(fprintf (stdout, "\n"));

  DVDCloseFile (file);

  g_object_unref (stream);

  if (copied != size)
  {
    LOG (fprintf (stderr, "Error: Failed to copy IFO for VTS %u\n", vts));
    return copied;
  }

  return size;
}

static gssize
dvd_copy_bup (DvdcpyObject *obj, guint vts)
{
  GFile *gfile;
  GFileOutputStream *stream;

  char filename[FILENAME_MAX + 1];
  gssize size, copied;

  dvd_file_t *file;

  if ((size = dvd_file_size (obj->reader, vts, DVD_READ_INFO_BACKUP_FILE)) <= 0)
    return size;

  if (vts == 0)
    snprintf (filename, FILENAME_MAX, "%s/VIDEO_TS/VIDEO_TS.BUP", obj->output);
  else
    snprintf (filename, FILENAME_MAX, "%s/VIDEO_TS/VTS_%02u_0.BUP", obj->output, vts);

  gfile = g_file_new_for_path (filename);
  stream = g_file_replace (gfile, NULL, FALSE, 0, NULL, NULL);
  g_object_unref (gfile);

  if (!stream)
  {
    LOG(fprintf (stderr, "Error: Cannot open %s\n", filename));
    return -1;
  }

  file = DVDOpenFile (obj->reader, vts, DVD_READ_INFO_BACKUP_FILE);
  if (!file)
  {
    LOG(fprintf (stderr, "Error: Cannot open BUP for VTS %u\n", vts));
    g_object_unref (stream);
    return -1;
  }

  LOG(fprintf (stdout, "Copying BUP for VTS %u\n", vts));
  copied = dvd_copy_blocks (obj, file, 0, size, stream, DVD_DATA_TYPE_BUP);
  LOG(fprintf (stdout, "\n"));

  DVDCloseFile (file);

  g_object_unref (stream);

  if (copied != size)
  {
    LOG(fprintf (stderr, "Error: Failed to copy BUP for VTS %u\n", vts));
    return copied;
  }

  return size;
}

static gssize
dvd_copy_menu (DvdcpyObject *obj, guint vts)
{
  GFile *gfile;
  GFileOutputStream *stream;

  char filename[FILENAME_MAX + 1];
  ssize_t size, copied;

  dvd_file_t *file;

  if ((size = dvd_file_size (obj->reader, vts, DVD_READ_MENU_VOBS)) <= 0)
    return size;

  if (vts == 0)
    snprintf (filename, FILENAME_MAX, "%s/VIDEO_TS/VIDEO_TS.VOB", obj->output);
  else
    snprintf (filename, FILENAME_MAX, "%s/VIDEO_TS/VTS_%02u_0.VOB", obj->output, vts);

  gfile = g_file_new_for_path (filename);
  stream = g_file_replace (gfile, NULL, FALSE, 0, NULL, NULL);
  g_object_unref (gfile);

  if (!stream)
  {
    LOG(fprintf (stderr, "Error: Cannot open %s\n", filename));
    return -1;
  }

  file = DVDOpenFile (obj->reader, vts, DVD_READ_MENU_VOBS);
  if (!file)
  {
    LOG(fprintf (stderr, "Error: Cannot open menu for VTS %u\n", vts));
    g_object_unref (stream);
    return -1;
  }

  LOG(fprintf (stdout, "Copying menu for VTS %u\n", vts));
  copied = dvd_copy_blocks (obj, file, 0, size, stream, DVD_DATA_TYPE_MENU);
  LOG(fprintf (stdout, "\n"));

  DVDCloseFile (file);

  g_object_unref (stream);

  if (copied != size)
  {
    LOG(fprintf (stderr, "Error: Failed to copy menu for VTS %u\n", vts));
    return copied;
  }

  return size;
}

static gssize
dvd_copy_vob (DvdcpyObject *obj, guint vts)
{
  GFile *gfile;
  GFileOutputStream *stream;

  gchar filename[FILENAME_MAX + 1];
  gssize size, copied, offset;
  gint vob;

  dvd_file_t *dvd_file;
  dvd_stat_t dvd_stat;

  if (DVDFileStat (obj->reader, vts, DVD_READ_TITLE_VOBS, &dvd_stat) < 0)
  {
    LOG(fprintf (stderr, "Error: Cannot stat VOB for VTS %u\n", vts));
    return -1;
  }

  dvd_file = DVDOpenFile (obj->reader, vts, DVD_READ_TITLE_VOBS);
  if (!dvd_file)
  {
    LOG(fprintf (stderr, "Error: Cannot open VOB for VTS %u\n", vts));
    return -1;
  }

  copied = -1;
  for (vob = 0, offset = 0; vob < dvd_stat.nr_parts; vob ++)
  {
    snprintf (filename, FILENAME_MAX, "%s/VIDEO_TS/VTS_%02u_%u.VOB", obj->output, vts, vob + 1);

    gfile = g_file_new_for_path (filename);
    stream = g_file_replace (gfile, NULL, FALSE, 0, NULL, NULL);
    g_object_unref (gfile);

    if (!stream)
    {
      DVDCloseFile (dvd_file);

      LOG(fprintf (stderr, "Error: Cannot open %s\n", filename));
      return -1;
    }

    size = dvd_stat.parts_size[vob] / DVD_VIDEO_LB_LEN;

    LOG(fprintf (stdout, "Copying VOB %u for VTS %u\n", vob + 1, vts));
    copied = dvd_copy_blocks (obj, dvd_file, offset, size, stream, DVD_DATA_TYPE_VOB);
    LOG(fprintf (stdout, "\n"));

    g_object_unref (stream);

    if (copied != size)
    {
      LOG(fprintf (stderr, "Error: Failed to copy VOB part %u for VTS %u\n", vob, vts));
      break;
    }

    offset += size;
  }

  DVDCloseFile (dvd_file);

  if (copied < 0 || offset != dvd_stat.size / (uint32_t) DVD_VIDEO_LB_LEN)
  {
    LOG(fprintf (stderr, "Error: Failed to copy VOB for VTS %u\n", vts));
    return copied;
  }

  return offset;
}

static gssize
dvd_copy_vmg (DvdcpyObject *obj)
{
  gssize fullsize, size;

  if ((size = dvd_copy_ifo (obj, 0)) < 0)
    return size;
  fullsize = size;

  if ((size = dvd_copy_bup (obj, 0)) > 0)
    fullsize += size;

  if ((size = dvd_copy_menu (obj, 0)) > 0)
    fullsize += size;

  return fullsize;
}

static gssize
dvd_copy_vts (DvdcpyObject *obj, guint vts)
{
  gssize fullsize, size;

  if ((size = dvd_copy_ifo (obj, vts)) < 0)
    return size;
  fullsize = size;

  if ((size = dvd_copy_bup (obj, vts)) > 0)
    fullsize += size;

  if ((size = dvd_copy_menu (obj, vts)) > 0)
    fullsize += size;

  if ((size = dvd_copy_vob (obj, vts)) < 0)
    return -1;
  fullsize += size;

  return fullsize;
}

static gssize
dvd_copy (DvdcpyObject *obj)
{
  gssize fullsize = 0, size;
  guint vts;

  if (obj->with_menus)
  {
    if ((size = dvd_copy_vmg (obj)) < 0)
      return size;
    fullsize = size;
  }

  for (vts = 0; vts < obj->vmg_file->vmgi_mat->vmg_nr_of_title_sets; vts++)
  {
    if (obj->vtss[vts])
    {
      if ((size = dvd_copy_vts (obj, vts+1)) < 0)
        return size;
      fullsize += size;
    }
  }

  return fullsize;
}

void
dvdcpy_object_set_strategy (DvdcpyObject *obj, BadBlockStrategy strategy)
{
  g_return_if_fail (DVDCPY_IS_OBJECT (obj));

  obj->strategy = strategy;
}

BadBlockStrategy
dvdcpy_object_get_strategy (DvdcpyObject *obj)
{
  g_return_val_if_fail (DVDCPY_IS_OBJECT (obj), BAD_BLOCK_STRATEGY_ABORT);

  return obj->strategy;
}

void
dvdcpy_object_set_copy_menus (DvdcpyObject *obj, gboolean with_menus)
{
  g_return_if_fail (DVDCPY_IS_OBJECT (obj));

  obj->with_menus = with_menus;
}

gboolean
dvdcpy_object_open (DvdcpyObject *obj, GFile *device, GList *titles, GError **error)
{
  gchar *path;
  guint i;

  g_return_val_if_fail (DVDCPY_IS_OBJECT (obj), FALSE);
  g_return_val_if_fail (G_IS_FILE (device), FALSE);
  g_return_val_if_fail (obj->is_opened == FALSE, FALSE);

  obj->is_opened = TRUE;

  path = g_file_get_path (device);
  obj->reader = DVDOpen (path);
  g_free (path);

  if (obj->reader == NULL)
  {
    g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED, _("Could not access DVD drive"));
    dvdcpy_object_close (obj);
    return FALSE;
  }

  obj->vmg_file = ifoOpen (obj->reader, 0);
  if (obj->vmg_file == NULL)
  {
    g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED, _("Could not access DVD drive"));
    dvdcpy_object_close (obj);
    return FALSE;
  }

  if (!titles)
    obj->with_menus = 1;
  else
  {
    GList *l;
    for (l = titles; l != NULL; l = l->next)
    {
      guint title = GPOINTER_TO_UINT (l->data);
      obj->titles[title - 1] = 1;
    }
  }

  for (i = 0; i < 100; i++)
  {
    if (obj->titles[i] && i >= obj->vmg_file->tt_srpt->nr_of_srpts)
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED, _("Title %d is not a valid title"), i + 1);
      dvdcpy_object_close (obj);
      return FALSE;
    }
  }

  memset (obj->vtss, 0, obj->vmg_file->vmgi_mat->vmg_nr_of_title_sets * sizeof (guint));
  for (i = 0; i < obj->vmg_file->tt_srpt->nr_of_srpts; i++)
    obj->vtss[obj->vmg_file->tt_srpt->title[i].title_set_nr - 1] |= !titles || obj->titles[i];

  obj->size = dvd_get_size (obj);
  if (obj->size < 0)
  {
    g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED, _("Could not query DVD size"));
    dvdcpy_object_close (obj);
    return FALSE;
  }

  return TRUE;
}

static void
dvdcpy_object_class_init (DvdcpyObjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = dvdcpy_object_finalize;

  signals [SKIPPED_SIGNAL] =
    g_signal_new ("skipped",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2, G_TYPE_UINT64, G_TYPE_UINT64);
  signals [PROGRESS_SIGNAL] =
    g_signal_new ("progress",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT64, G_TYPE_UINT64);
}

static void
dvdcpy_object_init (DvdcpyObject *obj)
{
  obj->with_menus = TRUE;
  obj->strategy = BAD_BLOCK_STRATEGY_ABORT;
}

DvdcpyObject *
dvdcpy_object_new (void)
{
  return g_object_new (DVDCPY_TYPE_OBJECT, NULL);
}

void
dvdcpy_object_close (DvdcpyObject *obj)
{
  g_return_if_fail (DVDCPY_IS_OBJECT (obj));
  g_return_if_fail (obj->is_opened != FALSE);

  memset (obj->titles, 0, 100 * sizeof (guint));

  g_free (obj->output);

  if (obj->vmg_file != NULL)
  {
    ifoClose (obj->vmg_file);
    obj->vmg_file = NULL;
  }

  if (obj->reader != NULL)
  {
    DVDClose (obj->reader);
    obj->reader = NULL;
  }

  obj->is_opened = FALSE;
}

static void
dvdcpy_object_finalize (GObject *object)
{
  DvdcpyObject *obj;

  g_return_if_fail (object != NULL);

  obj = DVDCPY_OBJECT (object);
  if (obj->is_opened != FALSE)
    dvdcpy_object_close (obj);

  G_OBJECT_CLASS (dvdcpy_object_parent_class)->finalize (G_OBJECT (obj));
}

gboolean
dvdcpy_object_copy (DvdcpyObject *obj, GFile *path, GError **error)
{
  GFile *ts_dir;
  guint64 space_left;
  ssize_t copied;

  g_return_val_if_fail (DVDCPY_IS_OBJECT (obj), FALSE);
  g_return_val_if_fail (G_IS_FILE (path), FALSE);
  g_return_val_if_fail (obj->is_copying == FALSE, FALSE);

  if (!get_space_left (path, &space_left))
  {
    g_set_error (error, G_IO_ERROR, G_IO_ERROR_NO_SPACE,
        _("Not enough space left on device (%s needed)"),
        g_format_size (obj->size * DVD_VIDEO_LB_LEN));
    return FALSE;
  }

  if (obj->size * DVD_VIDEO_LB_LEN > space_left)
  {
    g_set_error (error, G_IO_ERROR, G_IO_ERROR_NO_SPACE,
        _("Not enough space left on device (%s needed, %s available)"),
        g_format_size (obj->size * DVD_VIDEO_LB_LEN),
        g_format_size (space_left));
    return FALSE;
  }

  ts_dir = g_file_get_child (path, "VIDEO_TS");
  if (!make_directory (ts_dir, error))
  {
    g_object_unref (ts_dir);
    return FALSE;
  }
  g_object_unref (ts_dir);

  obj->is_copying = TRUE;

  if (obj->output)
    g_free (obj->output);
  obj->output = g_file_get_path (path);

  copied = dvd_copy (obj);
  if (copied < 0)
  {
    obj->is_copying = FALSE;
    if (copied == -1)
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED,
          _("An error occurred reading the DVD"));
    else
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED,
          _("An error occurred whilst writing the DVD to disk"));
    return FALSE;
  }

  obj->is_copying = FALSE;

  return TRUE;
}

void
dvdcpy_set_debug (gboolean debug)
{
  debug_enabled = debug;
}
